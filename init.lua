minetest.register_craft({
    output = 'default:gravel',
    recipe = {
        {"default:cobble"},
}
})
minetest.register_craft({
    output = 'default:gravel',
    recipe = {
        {"default:desert_cobble"},
}
})
minetest.register_craft({
    output = 'default:sand',
    recipe = {
        {"default:gravel"},
}
})
minetest.register_craft({
    output = 'default:sand',
    recipe = {
        {"default:gravel"},
}
})
minetest.register_craft({
    output = 'default:desert_sand',
    recipe = {
        {"default:sand"},
}
})
minetest.register_craft({
    output = 'default:sand',
    recipe = {
        {"default:desert_sand"},
}
})
